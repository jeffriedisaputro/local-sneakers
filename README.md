# Local Pride Sneakers

Website ini dibuat menggunakan Javascript Library REACT.JS, UI Frameworks menggunakan TailwindCSS, Serta Node.JS


Cara menjalankan aplikasi ini pada komputer Anda:

1. Silahkan Clone repositori ini terlebih dahulu atau download secara manual

   `git clone git@gitlab.com:jeffriedisaputro/local-sneakers.git`

2. Install dependencies

   `npm install` atau `yarn install`

3. Jalankan aplikasi

   `npm start` atau `yarn start`

4. Tunggu, `webpack-dev-server` akan membuka aplikasi Anda secara otomatis pada browser

5. Selesai

Enjoy it and Thank you, Have a Nice Day!

Author/Build by: Jefri Edi Saputro
