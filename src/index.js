import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './pages/Home';
import Sneakers from './pages/Details';
import './css/App.css';
import './css/Custom.css';


const AppRouter = () => (
  <BrowserRouter>
    <Route exact path="/" component={Home} />
    <Route path="/sneakers/:id" component={Sneakers} />
  </BrowserRouter>
);

ReactDOM.render(<AppRouter />, document.getElementById('app'));

if (process.env.NODE_ENV === 'development') {
  module.hot.accept();
}
