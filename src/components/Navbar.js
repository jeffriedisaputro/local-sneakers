import React from 'react';
import logo from '../images/logo-localpride.png';


export class Navbar extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: false
		};

		this.toggleNavMenu = this.toggleNavMenu.bind(this);
	}

  

	toggleNavMenu() {
		this.setState({ isOpen: !this.state.isOpen });
	}

	render() {
		let menuIcon;

		if (this.state.isOpen) {
			menuIcon = (
				<path
					fill-rule="evenodd"
					d="M18.278 16.864a1 1 0 0 1-1.414 1.414l-4.829-4.828-4.828 4.828a1 1 0 0 1-1.414-1.414l4.828-4.829-4.828-4.828a1 1 0 0 1 1.414-1.414l4.829 4.828 4.828-4.828a1 1 0 1 1 1.414 1.414l-4.828 4.829 4.828 4.828z"
				/>
			);
		} else {
			menuIcon = (
				<path
					fill-rule="evenodd"
					d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"
				/>
			);
		}
		return (
			<header class="bg-gray-900 sm:flex sm:justify-between xl:px-32 sm:px-4 sm:py-3 sm:items-center">
				<header class="flex items-center justify-between px-4 py-3 sm:p-0">
					<div>
            <a href="/" title="Bangga Buatan Indonesia">
						  <img class="w-40" src={logo} alt="local pride" />
            </a>
					</div>
					<div class="sm:hidden">
						<button
							onClick={this.toggleNavMenu}
							type="button"
							class="text-gray-500 hover:text-white focus:text-white focus:outline-none"
						>
							<svg class="h-6 w-6 fill-current" viewBox="0 0 24 24">
								{menuIcon}
							</svg>
						</button>
					</div>
				</header>

				<div class={this.state.isOpen ? 'px-2 pt-2 pb-4 inline-block' : 'px-2 pt-2 pb-2 hidden sm:flex'}>
          <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
            <div class="mr-5 text-base lg:flex-grow">
              <a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-yellow-600 mr-4">
                Lokal Brand
              </a>
              <a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-yellow-600 mr-4">
                Tentang Kami
              </a>
              <a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-yellow-600">
                Hubungi Kami
              </a>
            </div>
            <div>
              <a href="#" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-white hover:bg-yellow-600 mt-4 lg:mt-0">Sign in</a>
            </div>
          </div>
				</div>

			</header>
		);
	}
}

