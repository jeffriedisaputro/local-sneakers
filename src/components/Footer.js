import React from 'react';
import Icofont from 'react-icofont';

export const Footer = () => (
<footer>
        <div class="myfooter mx-auto container py-8">
            <div class="container">
                <div class="row flex">
                    <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/3 mb-6 mr-8 py-2">
                        <h3 class="mb-4">Local Pride</h3>
                        <p class="text-title mb-4">Local Pride Sneakers adalah platform yang menghubungkan komunitas sneakers dengan percakapan mendalam tentang budaya yang sekarang menjadi arus utama untuk gerakan yang lebih baik. serta untuk mendukung industri ekonomi kreatif (UMKM) di bidang sepatu.</p>
                        <ul class="links">
                            <li><span><a href="#">Telepon   : 081-220058765</a></span></li>
                            <li><span><a href="#">Email     : localpridesneakers@gmail.com</a></span></li>
                        </ul>
                    </div> 

                    <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/4 mb-6 mr-8 py-2">
                        <h3 class="mb-4">Tentang Kami</h3>
                        <ul class="links">
                            <li><span><a href="#">Produk</a></span></li>
                            <li><span><a href="#">Berita</a></span></li>
                            <li><span><a href="#">Brands Lokal</a></span></li>
                            <li><span><a href="#">Industri Kreatif</a></span></li>
                        </ul>
                    </div>

                    <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/5 mb-6 py-2">

                        <h3 class="mb-4" >Sosial Media</h3>
                        <ul class="footer-sosial-icon">
                            <li>
                                <a class="sosial-icon" href="#">
                                    <Icofont icon="icofont-facebook"/>
                                </a>
                            </li>
                            <li>
                                <a class="sosial-icon" href="#">
                                    <Icofont icon="icofont-linkedin"/>
                                </a>
                            </li>
                            <li>
                                <a class="sosial-icon" href="#">
                                    <Icofont icon="icofont-instagram"/>
                                </a>
                            </li>
                            <li>
                                <a class="sosial-icon" href="#">
                                    <Icofont icon="icofont-twitter"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="copyright">
                <p> Copyright &copy; 2020 Jefri Edi Saputro All Rights Reserved</p>
            </div>
        </div>

    </footer>

);