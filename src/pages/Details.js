import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Navbar } from '../components/Navbar';
import { Footer } from '../components/Footer';
import { API_BASE_URL } from '../constant';

class Sneaker extends Component {
  static propTypes = {
    match: PropTypes.object,
  };

  constructor() { 
    super();

    this.state = {
      sneaker: {},
    };
  }

  componentDidMount() {
    const ID = this.props.match.params.id;
    axios
      .get(API_BASE_URL + '/sneakers/' + ID)
      .then(res => {
        this.setState({
          sneaker: res.data,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { sneaker } = this.state;
    return (
      <div>
        <Navbar />
        <p className="breadcrumbs">
          <Link to="/">Home</Link> / {sneaker.article}
        </p>
        <section className="sneaker-detail-container">
          <div className="sneaker-detail-cover-wrapper">
            <img className="sneaker-detail-cover-image" src={sneaker.cover} />
          </div>
          <div className="sneaker-detail-misc">
            <p className="sneaker-detail-title">{sneaker.article}</p>
            <p className="label-merk">{sneaker.merk}</p>
            <p className="mb-2"><strong>Tanggal Rilis</strong> {sneaker.release_date}</p>
            <p>{sneaker.description}</p>
          </div>
        </section>
        <Footer/>
      </div>
    );
  }
}

export default Sneaker;
