import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Navbar } from '../components/Navbar';
import { Footer } from '../components/Footer';
import hero from '../images/Banner-Heroku.jpg';
import { API_BASE_URL } from '../constant';

class Home extends Component {
  constructor() {
    super();

    this.state = {
      searchKeyword: '',
      sneakers: [],
      listOfSneakers: [],
    };

    this.handleKeywordChange = this.handleKeywordChange.bind(this);
  }

  componentDidMount() {
    axios
      .get(API_BASE_URL + '/sneakers')
      .then(res => {
        this.setState({
          sneakers: res.data,
          listOfSneakers: res.data,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleKeywordChange(e) {
    const { listOfSneakers } = this.state;
    const val = e.target.value;
    this.setState(
      {
        searchKeyword: val,
      },
      () => {
        const result = listOfSneakers.filter(sneaker => {
          if (sneaker.article.startsWith(val[0]) || sneaker.article.search(val) > -1) {
            return sneaker;
          }
        });
        this.setState({
          sneakers: result,
        });
      },
    );
  }

  render() {
    const { searchKeyword, sneakers } = this.state;
    return (
      <div>
        <Navbar />

        <section className="Hero-Banner relative height-480  py-16 px-4">
          <div className="z-20 relative text-white container mx-auto">
            <h1 className="mb-4">Komunitas Sneakers Indonesia #BanggaBuatanIndonesia</h1>
            <p className="leading-normal">Local Pride Sneakers adalah platform yang menghubungkan komunitas sneakers dengan percakapan mendalam tentang budaya yang sekarang menjadi arus utama untuk gerakan yang lebih baik. serta untuk mendukung industri ekonomi kreatif (UMKM) di bidang sepatu.</p>
            
            <div className="search-box">
              <input
                type="text"
                className="search-input p-4 text-gray-700 w-full bg-white border border-gray-300 rounded-lg focus:outline-none focus:border-gray-400"
                value={searchKeyword}
                onChange={this.handleKeywordChange}
                placeholder="Cari Nama Lokal Sneakers "
              />
            </div>
          </div>

          <div className="absolute inset-0 h-full z-10">
            <img src={hero} alt="" class="h-full w-full object-fit-cover"/>
          </div>
        </section>

        
        <section className="books-container container py-16">
          {sneakers.map(sneaker => {
            return (
                <div key={sneaker.id} className="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/4 mb-6 m-1 py-2">
                  <img src={sneaker.cover} className="sneaker-image" />
                  <p className="sneaker-article">{sneaker.article}</p>
                  <div className="sneaker-value-wrapper">
                    <p className="price">{sneaker.price}</p>
                    <p>Rating {sneaker.rating}</p>
                  </div>
                  <p className="Merk">{sneaker.merk}</p>
                  <Link to={`/sneakers/${sneaker.id}`}>
                    <button type="button" className="detail-show-btn bg-red-600">
                      Lihat
                    </button>
                  </Link>
                </div>      
            );
          })}
        </section>
        <Footer/>
      </div>
    );
  }
}

export default Home;
